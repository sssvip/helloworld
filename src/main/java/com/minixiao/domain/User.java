package com.minixiao.domain;


/**
 * The type User..
 */
public class User {

  //user name.
  private String name; //命名规范修正，已没有黄色警告

  /**
   * Gets name.
   *
   * @return the name
   */
  public String getName() {
    return name;
  }

  /**
   * Sets name.
   *
   * @param name the name
   * @return the name
   */
  public User setName(String name) {
    this.name = name;
    return this;
  }
}
